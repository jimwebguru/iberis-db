-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: Iberis
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additionalemailrecipients`
--

DROP TABLE IF EXISTS `additionalemailrecipients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `additionalemailrecipients` (
  `emailRecipientId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `emailAddress` varchar(150) NOT NULL,
  `transactionId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`emailRecipientId`),
  KEY `FK_additionalemailrecipients_1` (`transactionId`),
  CONSTRAINT `FK_additionalemailrecipients_1` FOREIGN KEY (`transactionId`) REFERENCES `emailtransactions` (`transactionId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `allowedurls`
--

DROP TABLE IF EXISTS `allowedurls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allowedurls` (
  `pageItemAssignmentId` bigint(20) unsigned NOT NULL,
  `url` varchar(350) NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_allowedurls_1_idx` (`pageItemAssignmentId`),
  CONSTRAINT `fk_allowedurls_1` FOREIGN KEY (`pageItemAssignmentId`) REFERENCES `pageitemassignments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blogentries`
--

DROP TABLE IF EXISTS `blogentries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogentries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `blogId` bigint(20) unsigned DEFAULT NULL,
  `headerImage` bigint(20) unsigned DEFAULT '0',
  `body` text,
  `header` text,
  `author` varchar(150) DEFAULT NULL,
  `publishedDate` timestamp NULL DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `bodyImage` bigint(20) unsigned DEFAULT '0',
  `bodyImageAlignment` int(10) unsigned DEFAULT NULL,
  `bodyImageWidth` int(10) unsigned DEFAULT '200',
  `headerImageMax` tinyint(1) DEFAULT '0',
  `showAuthorInfo` tinyint(1) DEFAULT '1',
  `published` tinyint(4) DEFAULT '1',
  `urlAlias` varchar(300) NOT NULL,
  `allowEditorStyles` tinyint(4) DEFAULT '0',
  `metaTitle` varchar(150) DEFAULT NULL,
  `metaDescription` varchar(800) DEFAULT NULL,
  `enableSocialSharing` tinyint(4) DEFAULT '0',
  `enableFacebook` tinyint(4) DEFAULT '0',
  `enableTwitter` tinyint(4) DEFAULT '0',
  `enableLinkedin` tinyint(4) DEFAULT '0',
  `enableEmail` tinyint(4) DEFAULT '0',
  `socialTitle` varchar(150) DEFAULT NULL,
  `socialDescription` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `urlAlias` varchar(200) NOT NULL,
  `readMoreLinkText` varchar(80) DEFAULT NULL,
  `showListThumbnails` tinyint(4) DEFAULT '0',
  `listThumbnailWidth` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `blogsites`
--

DROP TABLE IF EXISTS `blogsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogsites` (
  `siteId` bigint(20) unsigned NOT NULL,
  `blogId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`siteId`,`blogId`),
  KEY `fk_blogsites_1_idx` (`blogId`),
  CONSTRAINT `fk_blogsites_1` FOREIGN KEY (`blogId`) REFERENCES `blogs` (`id`),
  CONSTRAINT `fk_blogsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `canvas`
--

DROP TABLE IF EXISTS `canvas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canvas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conditionoperators`
--

DROP TABLE IF EXISTS `conditionoperators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conditionoperators` (
  `operatorId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `conditionName` varchar(50) NOT NULL,
  `operator` varchar(10) NOT NULL,
  PRIMARY KEY (`operatorId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `bannerGraphic` bigint(20) unsigned DEFAULT NULL,
  `body` mediumtext,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `urlAlias` varchar(150) DEFAULT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'Page',
  `body2` mediumtext,
  `body3` mediumtext,
  `body4` mediumtext,
  `header` mediumtext,
  `subHeader` mediumtext,
  `footer` mediumtext,
  `subFooter` mediumtext,
  `contentLayout` varchar(100) DEFAULT NULL,
  `footerLayout` varchar(100) DEFAULT NULL,
  `headerLayout` varchar(100) DEFAULT NULL,
  `pageTemplateId` bigint(20) unsigned DEFAULT NULL,
  `showTitle` tinyint(4) DEFAULT NULL,
  `titleSize` varchar(45) DEFAULT NULL,
  `showBlockTitle` tinyint(4) DEFAULT '0',
  `blockTitleSize` varchar(45) DEFAULT NULL,
  `showTitleAboveBanner` tinyint(4) DEFAULT NULL,
  `customContentTemplate` varchar(80) DEFAULT NULL,
  `galleryId` bigint(20) unsigned DEFAULT NULL,
  `presentationId` bigint(20) unsigned DEFAULT NULL,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  `unitGridId` bigint(20) unsigned DEFAULT NULL,
  `availableGlobal` tinyint(4) DEFAULT '0',
  `printable` tinyint(4) DEFAULT '0',
  `showFilesDownload` tinyint(4) DEFAULT '0',
  `filesHeaderText` varchar(250) DEFAULT NULL,
  `metaTitle` varchar(150) DEFAULT NULL,
  `metaDescription` varchar(800) DEFAULT NULL,
  `allowEditorStyles` tinyint(4) DEFAULT '0',
  `showNotes` tinyint(4) DEFAULT '0',
  `blocksSectionType` int(10) unsigned DEFAULT '0',
  `scope` varchar(100) DEFAULT NULL,
  `showOnlyScope` tinyint(4) DEFAULT '0',
  `enableSocialSharing` tinyint(4) DEFAULT '0',
  `enableFacebook` tinyint(4) DEFAULT '0',
  `enableTwitter` tinyint(4) DEFAULT '0',
  `enableLinkedin` tinyint(4) DEFAULT '0',
  `enableEmail` tinyint(4) DEFAULT '0',
  `socialTitle` varchar(150) DEFAULT NULL,
  `socialDescription` varchar(800) DEFAULT NULL,
  `overrideRegions` tinyint(4) DEFAULT '0',
  `centerRegions` tinyint(4) DEFAULT '0',
  `useCustomHtml` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `content_fulltext_ndx` (`title`,`header`,`subHeader`,`body`,`body2`,`body3`,`body4`,`footer`,`subFooter`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contentblocks`
--

DROP TABLE IF EXISTS `contentblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentblocks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sectionId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pageItem` bigint(20) unsigned NOT NULL DEFAULT '0',
  `pageItemInstanceId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `width` int(10) unsigned DEFAULT NULL,
  `sequence` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contentblocksections`
--

DROP TABLE IF EXISTS `contentblocksections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentblocksections` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contentId` bigint(20) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `sequence` int(11) DEFAULT '0',
  `showTitle` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contentfiles`
--

DROP TABLE IF EXISTS `contentfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentfiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `contentId` bigint(20) unsigned NOT NULL,
  `fileId` bigint(20) unsigned NOT NULL,
  `sequence` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_contentfiles_1_idx` (`contentId`),
  KEY `fk_contentfiles_2_idx` (`fileId`),
  CONSTRAINT `fk_contentfiles_1` FOREIGN KEY (`contentId`) REFERENCES `content` (`id`),
  CONSTRAINT `fk_contentfiles_2` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contentsites`
--

DROP TABLE IF EXISTS `contentsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contentsites` (
  `siteId` bigint(20) unsigned NOT NULL,
  `contentId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`siteId`,`contentId`),
  KEY `fk_contentsites_1_idx` (`contentId`),
  CONSTRAINT `fk_contentsites_1` FOREIGN KEY (`contentId`) REFERENCES `content` (`id`),
  CONSTRAINT `fk_contentsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contenttags`
--

DROP TABLE IF EXISTS `contenttags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenttags` (
  `contentId` bigint(20) unsigned NOT NULL,
  `tagId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`contentId`,`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `deniedurls`
--

DROP TABLE IF EXISTS `deniedurls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deniedurls` (
  `pageItemAssignmentId` bigint(20) unsigned NOT NULL,
  `url` varchar(350) NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_deniedurls_1_idx` (`pageItemAssignmentId`),
  CONSTRAINT `fk_deniedurls_1` FOREIGN KEY (`pageItemAssignmentId`) REFERENCES `pageitemassignments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dynamic_car`
--

DROP TABLE IF EXISTS `dynamic_car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_car` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `carName` varchar(200) DEFAULT NULL,
  `carOwner` bigint(20) unsigned DEFAULT '0',
  `association` bigint(20) unsigned DEFAULT '0',
  `associationInstanceId` bigint(20) unsigned DEFAULT '0',
  `description` text,
  `carCode` text,
  `transmissiontype` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dynamicfieldlistitems`
--

DROP TABLE IF EXISTS `dynamicfieldlistitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamicfieldlistitems` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fieldId` bigint(20) unsigned NOT NULL,
  `itemValue` varchar(100) DEFAULT NULL,
  `displayValue` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dynamicformfields`
--

DROP TABLE IF EXISTS `dynamicformfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamicformfields` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `formId` bigint(20) unsigned NOT NULL,
  `sectionId` bigint(20) unsigned DEFAULT NULL,
  `label` varchar(250) NOT NULL,
  `fieldType` bigint(20) NOT NULL,
  `unitId` bigint(20) DEFAULT NULL,
  `gridId` bigint(20) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `lookupUnitId` bigint(20) DEFAULT NULL,
  `lookupGridId` bigint(20) DEFAULT NULL,
  `required` tinyint(4) DEFAULT '0',
  `inlineLabel` tinyint(4) DEFAULT '0',
  `requiredMessage` varchar(250) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `cssClasses` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dynamicforms`
--

DROP TABLE IF EXISTS `dynamicforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamicforms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `useSiteEmail` tinyint(4) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `confirmation` text NOT NULL,
  `showTitle` tinyint(4) DEFAULT NULL,
  `submitButtonText` varchar(100) NOT NULL,
  `header` text,
  `footer` text,
  `sectionsType` int(11) DEFAULT NULL,
  `sendEmail` tinyint(4) DEFAULT NULL,
  `emailTemplate` text,
  `confirmationEmailTemplate` text,
  `urlAlias` varchar(150) DEFAULT NULL,
  `titleSize` varchar(25) DEFAULT NULL,
  `pageTemplateId` bigint(20) unsigned DEFAULT NULL,
  `recipientEmailField` bigint(20) unsigned DEFAULT NULL,
  `metaTitle` varchar(150) DEFAULT NULL,
  `metaDescription` varchar(800) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `customContentTemplate` varchar(100) DEFAULT NULL,
  `buttonCssClasses` varchar(100) DEFAULT NULL,
  `useCaptcha` tinyint(4) DEFAULT '0',
  `sendConfirmationEmail` tinyint(4) DEFAULT '0',
  `emailSubject` varchar(250) DEFAULT NULL,
  `confirmationEmailSubject` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dynamicformsections`
--

DROP TABLE IF EXISTS `dynamicformsections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamicformsections` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `formId` bigint(20) unsigned DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `header` text,
  `footer` text,
  `sequence` int(11) DEFAULT NULL,
  `showTitle` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dynamicformsites`
--

DROP TABLE IF EXISTS `dynamicformsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamicformsites` (
  `formId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`formId`,`siteId`),
  KEY `fk_dynamicformsites_2_idx` (`siteId`),
  CONSTRAINT `fk_dynamicformsites_1` FOREIGN KEY (`formId`) REFERENCES `dynamicforms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dynamicformsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailtransactions`
--

DROP TABLE IF EXISTS `emailtransactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailtransactions` (
  `transactionId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) NOT NULL,
  `body` text NOT NULL,
  `fromEmail` varchar(150) NOT NULL,
  `siteId` bigint(20) unsigned DEFAULT '0',
  `toEmail` varchar(150) NOT NULL,
  `emailSent` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`transactionId`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faqcategory`
--

DROP TABLE IF EXISTS `faqcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqcategory` (
  `faq_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`faq_id`,`term_id`),
  KEY `fk_FAQ_has_Term_FAQ_Category_FAQ1_idx` (`faq_id`),
  KEY `fk_faqcategory_2_idx` (`term_id`),
  CONSTRAINT `fk_faqcategory_1` FOREIGN KEY (`faq_id`) REFERENCES `faqs` (`id`),
  CONSTRAINT `fk_faqcategory_2` FOREIGN KEY (`term_id`) REFERENCES `vocabterms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `question` text,
  `answer` text,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fileextensions`
--

DROP TABLE IF EXISTS `fileextensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fileextensions` (
  `extensionid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`extensionid`)
) ENGINE=InnoDB AUTO_INCREMENT=448 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(200) DEFAULT NULL,
  `data` blob,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `display_name` varchar(200) DEFAULT NULL,
  `mimetypeid` bigint(20) unsigned NOT NULL,
  `fileextensionid` bigint(20) unsigned NOT NULL,
  `filePath` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filesites`
--

DROP TABLE IF EXISTS `filesites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filesites` (
  `fileId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`fileId`,`siteId`),
  KEY `fk_filesites_2_idx` (`siteId`),
  CONSTRAINT `fk_filesites_1` FOREIGN KEY (`fileId`) REFERENCES `files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_filesites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `filetags`
--

DROP TABLE IF EXISTS `filetags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filetags` (
  `fileId` bigint(20) unsigned NOT NULL,
  `tagId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`fileId`,`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `forms`
--

DROP TABLE IF EXISTS `forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms` (
  `formId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  `formType` bigint(20) unsigned DEFAULT NULL,
  `layoutXml` text,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`formId`),
  KEY `Forms_idx` (`formType`),
  KEY `Forms_idx2` (`unitId`),
  CONSTRAINT `Forms_ibfk_1` FOREIGN KEY (`formType`) REFERENCES `formtypes` (`formTypeId`),
  CONSTRAINT `Forms_ibfk_2` FOREIGN KEY (`unitId`) REFERENCES `systemunits` (`unitID`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formsubmissions`
--

DROP TABLE IF EXISTS `formsubmissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formsubmissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `submission` text NOT NULL,
  `formId` bigint(20) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formtypes`
--

DROP TABLE IF EXISTS `formtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formtypes` (
  `formTypeId` bigint(20) unsigned NOT NULL,
  `formTypeName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`formTypeId`),
  UNIQUE KEY `formTypeName` (`formTypeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `imagesPerSlide` int(11) DEFAULT NULL,
  `showThumbnails` tinyint(4) DEFAULT '0',
  `showCaptions` tinyint(4) DEFAULT NULL,
  `animationSpeed` int(11) NOT NULL DEFAULT '0',
  `startSlide` int(11) DEFAULT '0',
  `showPager` tinyint(4) DEFAULT '0',
  `showControls` tinyint(4) DEFAULT '0',
  `autoSlide` tinyint(4) DEFAULT '0',
  `autoStart` tinyint(4) DEFAULT '0',
  `pauseTime` int(11) NOT NULL DEFAULT '1000',
  `minHeight` int(11) NOT NULL DEFAULT '0',
  `minWidth` int(11) NOT NULL DEFAULT '0',
  `maxWidth` int(11) NOT NULL DEFAULT '0',
  `maxHeight` int(11) NOT NULL DEFAULT '0',
  `showName` tinyint(4) DEFAULT NULL,
  `imageWidth` int(11) DEFAULT '0',
  `imageMaxWidth` tinyint(4) DEFAULT '0',
  `blockSize` int(11) DEFAULT '0',
  `alternatingBlockSize` int(11) DEFAULT '0',
  `visibleThumbs` int(11) DEFAULT '4',
  `flipSlides` tinyint(4) DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `captionAsOverlay` tinyint(4) DEFAULT '0',
  `slideMode` varchar(45) DEFAULT NULL,
  `autoHideControls` tinyint(4) DEFAULT '0',
  `allowEditorStyles` tinyint(4) DEFAULT '0',
  `showCaptionsBelowGallery` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `galleryimages`
--

DROP TABLE IF EXISTS `galleryimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleryimages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `galleryId` bigint(20) unsigned NOT NULL,
  `imageId` bigint(20) unsigned NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `caption` text,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_galleryimages_1_idx` (`galleryId`),
  KEY `fk_galleryimages_2_idx` (`imageId`),
  CONSTRAINT `fk_galleryimages_1` FOREIGN KEY (`galleryId`) REFERENCES `galleries` (`id`),
  CONSTRAINT `fk_galleryimages_2` FOREIGN KEY (`imageId`) REFERENCES `images` (`imageid`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gallerysites`
--

DROP TABLE IF EXISTS `gallerysites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallerysites` (
  `galleryId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`galleryId`,`siteId`),
  KEY `fk_gallerysites_2_idx` (`siteId`),
  CONSTRAINT `fk_gallerysites_1` FOREIGN KEY (`galleryId`) REFERENCES `galleries` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gallerysites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gridcolumns`
--

DROP TABLE IF EXISTS `gridcolumns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gridcolumns` (
  `gridColumnId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unitProperty` bigint(20) unsigned DEFAULT '0',
  `gridId` bigint(20) unsigned DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  PRIMARY KEY (`gridColumnId`),
  KEY `GridColumns_idx` (`gridId`),
  KEY `unitProperty` (`unitProperty`),
  CONSTRAINT `GridColumns_ibfk_1` FOREIGN KEY (`gridId`) REFERENCES `grids` (`gridId`),
  CONSTRAINT `GridColumns_ibfk_2` FOREIGN KEY (`unitProperty`) REFERENCES `unitproperties` (`propertyId`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grids`
--

DROP TABLE IF EXISTS `grids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grids` (
  `gridId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gridName` varchar(50) DEFAULT NULL,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`gridId`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `icons`
--

DROP TABLE IF EXISTS `icons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3977 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `imageid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `width` bigint(20) unsigned DEFAULT '0',
  `height` bigint(20) unsigned DEFAULT '0',
  `mimetypeid` bigint(20) unsigned NOT NULL,
  `fileextensionid` bigint(20) unsigned NOT NULL,
  `imagecontents` blob,
  `perspective` bigint(20) unsigned DEFAULT '0',
  `filePath` varchar(200) DEFAULT NULL,
  `caption` varchar(1000) DEFAULT NULL,
  `showCaption` tinyint(4) DEFAULT '0',
  `captionAsOverlay` tinyint(4) DEFAULT '0',
  `overlayColor` varchar(45) DEFAULT NULL,
  `captionTextColor` varchar(45) DEFAULT NULL,
  `showFullWidth` tinyint(4) DEFAULT '0',
  `fullOverlay` tinyint(4) DEFAULT '0',
  `sideOverlay` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`imageid`),
  KEY `FK_images_1` (`fileextensionid`),
  KEY `FK_images_2` (`mimetypeid`),
  CONSTRAINT `FK_images_1` FOREIGN KEY (`fileextensionid`) REFERENCES `fileextensions` (`extensionid`),
  CONSTRAINT `FK_images_2` FOREIGN KEY (`mimetypeid`) REFERENCES `mimetypes` (`typeid`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imagesites`
--

DROP TABLE IF EXISTS `imagesites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagesites` (
  `imageId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`imageId`,`siteId`),
  KEY `fk_imagesites_2_idx` (`siteId`),
  CONSTRAINT `fk_imagesites_1` FOREIGN KEY (`imageId`) REFERENCES `images` (`imageid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_imagesites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imagetags`
--

DROP TABLE IF EXISTS `imagetags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagetags` (
  `imageId` bigint(20) unsigned NOT NULL,
  `tagId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`imageId`,`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imagetypes`
--

DROP TABLE IF EXISTS `imagetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagetypes` (
  `typeid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `layoutpageregions`
--

DROP TABLE IF EXISTS `layoutpageregions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layoutpageregions` (
  `pageRegionId` bigint(20) unsigned NOT NULL,
  `layoutId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`pageRegionId`,`layoutId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `layouts`
--

DROP TABLE IF EXISTS `layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layouts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `templateDirName` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturers` (
  `manufacturerId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`manufacturerId`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manufacturersites`
--

DROP TABLE IF EXISTS `manufacturersites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturersites` (
  `manufacturerId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`manufacturerId`,`siteId`),
  KEY `fk_manufacturersites_2_idx` (`siteId`),
  CONSTRAINT `fk_manufacturersites_1` FOREIGN KEY (`manufacturerId`) REFERENCES `manufacturers` (`manufacturerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_manufacturersites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `membershiproles`
--

DROP TABLE IF EXISTS `membershiproles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membershiproles` (
  `membershipId` bigint(20) unsigned NOT NULL,
  `roleId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`membershipId`,`roleId`),
  KEY `fk_membershiproles_2_idx` (`roleId`),
  CONSTRAINT `fk_membershiproles_1` FOREIGN KEY (`membershipId`) REFERENCES `sitemembership` (`membershipID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_membershiproles_2` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menucolumns`
--

DROP TABLE IF EXISTS `menucolumns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menucolumns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sequenceIndex` int(11) DEFAULT NULL,
  `parentMenuItem` bigint(20) unsigned DEFAULT '0',
  `customContent` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menuitems`
--

DROP TABLE IF EXISTS `menuitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menuitems` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(150) DEFAULT NULL,
  `displayName` varchar(100) DEFAULT NULL,
  `cssClassNames` varchar(100) DEFAULT NULL,
  `parentMenuItem` bigint(20) unsigned DEFAULT NULL,
  `parentMenu` bigint(20) unsigned DEFAULT NULL,
  `subMenuGroup` tinyint(1) DEFAULT '0',
  `sequenceIndex` int(11) DEFAULT '0',
  `anonymousOnly` tinyint(1) DEFAULT '0',
  `icon` varchar(80) DEFAULT NULL,
  `menuColumn` bigint(20) unsigned DEFAULT '0',
  `separatorBefore` tinyint(4) DEFAULT '0',
  `separatorAfter` tinyint(4) DEFAULT '0',
  `externalLink` tinyint(4) DEFAULT '0',
  `openNewTab` tinyint(4) DEFAULT '0',
  `width` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_menuitem_menu` (`parentMenuItem`),
  KEY `fk_MenuItems_menu_idx` (`parentMenu`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menuitemsroles`
--

DROP TABLE IF EXISTS `menuitemsroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menuitemsroles` (
  `MenuItem_id` bigint(20) unsigned NOT NULL,
  `Role_id` bigint(20) unsigned NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Idx_Item_Role` (`MenuItem_id`,`Role_id`),
  KEY `fk_role_idx` (`Role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `orientation` varchar(45) DEFAULT NULL,
  `buttonText` varchar(60) DEFAULT NULL,
  `width` int(11) DEFAULT '0',
  `subWidth` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `createMobile` tinyint(4) DEFAULT '0',
  `menuToCrumb` bigint(20) unsigned DEFAULT '0',
  `toggleable` tinyint(4) DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `zIndex` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menusites`
--

DROP TABLE IF EXISTS `menusites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menusites` (
  `menuId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`menuId`,`siteId`),
  KEY `fk_menusites_2_idx` (`siteId`),
  CONSTRAINT `fk_menusites_1` FOREIGN KEY (`menuId`) REFERENCES `menus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_menusites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mimetypes`
--

DROP TABLE IF EXISTS `mimetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mimetypes` (
  `typeid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) DEFAULT NULL,
  `content` text,
  `unitId` bigint(20) unsigned NOT NULL,
  `instanceId` bigint(20) unsigned NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pageitemassignments`
--

DROP TABLE IF EXISTS `pageitemassignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pageitemassignments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pageRegion` bigint(20) unsigned NOT NULL,
  `pageItem` bigint(20) unsigned NOT NULL,
  `pageItemInstanceId` bigint(20) unsigned DEFAULT '0',
  `siteId` bigint(20) unsigned NOT NULL,
  `sequence` int(11) DEFAULT '0',
  `printable` tinyint(4) DEFAULT '0',
  `mobileOnly` tinyint(4) DEFAULT '0',
  `desktopOnly` tinyint(4) DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `removeSpacer` tinyint(4) DEFAULT '0',
  `scope` varchar(100) DEFAULT NULL,
  `beginWrapperHtml` text,
  `endWrapperHtml` text,
  `useWrapper` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pageregions`
--

DROP TABLE IF EXISTS `pageregions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pageregions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `globalPermission` tinyint(1) DEFAULT '0',
  `canRead` tinyint(1) DEFAULT '0',
  `canUpdate` tinyint(1) DEFAULT '0',
  `canCreate` tinyint(1) DEFAULT '0',
  `canDelete` tinyint(1) DEFAULT '0',
  `roleId` bigint(20) unsigned DEFAULT NULL,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  `siteId` bigint(20) unsigned DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_roles_id_fk` (`roleId`),
  KEY `permissions_systemunits_unitID_fk` (`unitId`),
  CONSTRAINT `permissions_roles_id_fk` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`),
  CONSTRAINT `permissions_systemunits_unitID_fk` FOREIGN KEY (`unitId`) REFERENCES `systemunits` (`unitID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `presentations`
--

DROP TABLE IF EXISTS `presentations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `presentationType` int(11) NOT NULL,
  `toggleable` tinyint(4) NOT NULL DEFAULT '0',
  `animationSpeed` int(11) NOT NULL DEFAULT '0',
  `orientation` varchar(50) DEFAULT '0',
  `scrollable` tinyint(4) DEFAULT '0',
  `sectionsClosable` tinyint(4) DEFAULT '0',
  `slideMode` varchar(50) DEFAULT NULL,
  `startSlide` int(11) DEFAULT '0',
  `showPager` tinyint(4) DEFAULT '0',
  `showControls` tinyint(4) DEFAULT '0',
  `autoSlide` tinyint(4) DEFAULT '0',
  `autoStart` tinyint(4) DEFAULT '0',
  `pauseTime` int(11) NOT NULL DEFAULT '1000',
  `minHeight` int(11) NOT NULL DEFAULT '0',
  `minWidth` int(11) NOT NULL DEFAULT '0',
  `maxWidth` int(11) NOT NULL DEFAULT '0',
  `maxHeight` int(11) NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL,
  `showName` tinyint(4) DEFAULT NULL,
  `showSlideTitle` tinyint(4) DEFAULT NULL,
  `heroSliderMode` tinyint(4) DEFAULT NULL,
  `titleHeroStyle` varchar(500) DEFAULT NULL,
  `imageHeroStyle` varchar(500) DEFAULT NULL,
  `captionHeroStyle` varchar(500) DEFAULT NULL,
  `linkHeroStyle` varchar(500) DEFAULT NULL,
  `collapsed` varchar(45) DEFAULT NULL,
  `linkContainerHeroStyle` varchar(500) DEFAULT NULL,
  `imageMaxWidth` tinyint(4) DEFAULT '0',
  `blockSize` int(11) DEFAULT '0',
  `alternatingBlockSize` int(11) DEFAULT '0',
  `allowEditorStyles` tinyint(4) DEFAULT '0',
  `splitPane` tinyint(4) DEFAULT '0',
  `leftPaneSize` int(11) DEFAULT NULL,
  `paneReversed` tinyint(4) DEFAULT NULL,
  `flipSlides` tinyint(4) DEFAULT '0',
  `originalFormat` tinyint(4) DEFAULT '0',
  `primaryColor` varchar(45) DEFAULT NULL,
  `secondaryColor` varchar(45) DEFAULT NULL,
  `useVwFont` tinyint(4) DEFAULT '0',
  `useOverlay` tinyint(4) DEFAULT '0',
  `autoHideControls` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `presentationsites`
--

DROP TABLE IF EXISTS `presentationsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentationsites` (
  `presentationId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`presentationId`,`siteId`),
  KEY `fk_presentationsites_2_idx` (`siteId`),
  CONSTRAINT `fk_presentationsites_1` FOREIGN KEY (`presentationId`) REFERENCES `presentations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_presentationsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `presentationslides`
--

DROP TABLE IF EXISTS `presentationslides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentationslides` (
  `presentationId` bigint(20) unsigned NOT NULL,
  `slideId` bigint(20) unsigned NOT NULL,
  `sequence` int(11) NOT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `fk_presentationslides_1_idx` (`presentationId`),
  KEY `fk_presentationslides_2_idx` (`slideId`),
  CONSTRAINT `fk_presentationslides_1` FOREIGN KEY (`presentationId`) REFERENCES `presentations` (`id`),
  CONSTRAINT `fk_presentationslides_2` FOREIGN KEY (`slideId`) REFERENCES `slides` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productcataloggroups`
--

DROP TABLE IF EXISTS `productcataloggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcataloggroups` (
  `catalogId` bigint(20) unsigned NOT NULL,
  `groupId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`catalogId`,`groupId`),
  KEY `fk_productcataloggroups_2_idx` (`groupId`),
  CONSTRAINT `fk_productcataloggroups_1` FOREIGN KEY (`catalogId`) REFERENCES `productcatalogs` (`id`),
  CONSTRAINT `fk_productcataloggroups_2` FOREIGN KEY (`groupId`) REFERENCES `productgroups` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productcatalogproducts`
--

DROP TABLE IF EXISTS `productcatalogproducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcatalogproducts` (
  `catalogId` bigint(20) unsigned NOT NULL,
  `productId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`catalogId`,`productId`),
  KEY `fk_productcatalogproducts_2_idx` (`productId`),
  CONSTRAINT `fk_productcatalogproducts_1` FOREIGN KEY (`catalogId`) REFERENCES `productcatalogs` (`id`),
  CONSTRAINT `fk_productcatalogproducts_2` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productcatalogs`
--

DROP TABLE IF EXISTS `productcatalogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcatalogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `urlAlias` varchar(100) NOT NULL,
  `columns` int(11) DEFAULT '1',
  `siteId` bigint(20) unsigned DEFAULT '0',
  `customTemplate` varchar(100) DEFAULT NULL,
  `layoutMode` int(11) DEFAULT '0',
  `header` text,
  `subHeader` text,
  `footer` text,
  `subFooter` text,
  `showTitle` tinyint(4) DEFAULT NULL,
  `titleSize` varchar(45) DEFAULT NULL,
  `initialRows` int(11) DEFAULT '2',
  `pagerTemplate` varchar(80) DEFAULT '6,12,16',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productcatalogsites`
--

DROP TABLE IF EXISTS `productcatalogsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcatalogsites` (
  `catalogId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`catalogId`,`siteId`),
  KEY `fk_productcatalogsites_2_idx` (`siteId`),
  CONSTRAINT `fk_productcatalogsites_1` FOREIGN KEY (`catalogId`) REFERENCES `productcatalogs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productcatalogsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productgroups`
--

DROP TABLE IF EXISTS `productgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productgroups` (
  `groupId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(100) NOT NULL,
  `markup` double DEFAULT '0',
  `parentGroupId` bigint(20) unsigned DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=691 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productgroupsites`
--

DROP TABLE IF EXISTS `productgroupsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productgroupsites` (
  `groupId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`groupId`,`siteId`),
  KEY `fk_productgroupsites_2_idx` (`siteId`),
  CONSTRAINT `fk_productgroupsites_1` FOREIGN KEY (`groupId`) REFERENCES `productgroups` (`groupId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productgroupsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productimages`
--

DROP TABLE IF EXISTS `productimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productimages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productId` bigint(20) unsigned NOT NULL,
  `imageId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `productId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productName` varchar(100) NOT NULL,
  `productGroup` bigint(20) unsigned DEFAULT NULL,
  `manufacturerId` bigint(20) unsigned DEFAULT NULL,
  `details` text,
  `manufacturerPartNumber` varchar(100) NOT NULL,
  `mainImageId` bigint(20) unsigned DEFAULT NULL,
  `price` decimal(12,2) DEFAULT '0.00',
  `quantity` int(11) unsigned DEFAULT '0',
  `details2` text,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`productId`),
  UNIQUE KEY `products_unique_prodname` (`productName`),
  KEY `new_fk_products_productgroups` (`productGroup`),
  KEY `FK_Products_2` (`manufacturerId`),
  CONSTRAINT `FK_Products_2` FOREIGN KEY (`manufacturerId`) REFERENCES `manufacturers` (`manufacturerId`),
  CONSTRAINT `new_fk_products_productgroups` FOREIGN KEY (`productGroup`) REFERENCES `productgroups` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `productsites`
--

DROP TABLE IF EXISTS `productsites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productsites` (
  `productId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`productId`,`siteId`),
  KEY `fk_oroductsites_2_idx` (`siteId`),
  CONSTRAINT `fk_oroductsites_1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_oroductsites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resourcebundle`
--

DROP TABLE IF EXISTS `resourcebundle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resourcebundle` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bundleName` varchar(45) DEFAULT NULL,
  `bundleKey` varchar(100) NOT NULL,
  `bundleValue` varchar(1000) NOT NULL,
  `countryCode` varchar(10) NOT NULL,
  `languageCode` varchar(10) NOT NULL,
  `variant` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bundleKeyUnique` (`bundleKey`)
) ENGINE=InnoDB AUTO_INCREMENT=492 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `searchcolumns`
--

DROP TABLE IF EXISTS `searchcolumns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searchcolumns` (
  `searchID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unitProperty` bigint(20) unsigned NOT NULL,
  `gridId` bigint(20) unsigned DEFAULT NULL,
  `conditionOperatorId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `sequence` int(11) DEFAULT '0',
  PRIMARY KEY (`searchID`),
  KEY `SearchColumns_idx` (`gridId`),
  KEY `fk_searchcolumns_conditionoperator` (`conditionOperatorId`),
  KEY `fk_searchcolumns_unitproperty` (`unitProperty`),
  CONSTRAINT `SearchColumns_ibfk_1` FOREIGN KEY (`gridId`) REFERENCES `grids` (`gridId`),
  CONSTRAINT `fk_searchcolumns_conditionoperator` FOREIGN KEY (`conditionOperatorId`) REFERENCES `conditionoperators` (`operatorId`),
  CONSTRAINT `fk_searchcolumns_unitproperty` FOREIGN KEY (`unitProperty`) REFERENCES `unitproperties` (`propertyId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sitemembership`
--

DROP TABLE IF EXISTS `sitemembership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitemembership` (
  `membershipID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cancelDate` datetime DEFAULT NULL,
  `userId` bigint(20) unsigned DEFAULT NULL,
  `onlineDate` datetime DEFAULT NULL,
  `siteID` bigint(20) unsigned DEFAULT NULL,
  `status` bigint(20) unsigned DEFAULT NULL,
  `signupDate` datetime NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`membershipID`),
  KEY `fk_SiteMembership__clientID_Clients__clientID` (`userId`),
  KEY `fk_SiteMembership__siteID_Site__siteID` (`siteID`),
  KEY `fk_SiteMembership__status_Status__statusID` (`status`),
  CONSTRAINT `SiteMembership_ibfk_2` FOREIGN KEY (`siteID`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `SiteMembership_ibfk_3` FOREIGN KEY (`status`) REFERENCES `status` (`statusID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `siteID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `siteName` varchar(50) DEFAULT NULL,
  `siteType` bigint(20) unsigned DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `mailFrom` varchar(255) DEFAULT NULL,
  `mailHost` varchar(100) DEFAULT NULL,
  `mailPort` int(11) DEFAULT NULL,
  `mailUseSSL` tinyint(4) DEFAULT '0',
  `frontPageTemplateId` bigint(20) unsigned DEFAULT NULL,
  `innerPageTemplateId` bigint(20) unsigned DEFAULT NULL,
  `frontPageContent` bigint(20) unsigned DEFAULT NULL,
  `showFrontPageContent` tinyint(4) DEFAULT NULL,
  `logoId` bigint(20) unsigned DEFAULT NULL,
  `allowRegistration` tinyint(4) DEFAULT '0',
  `emailAsUsername` tinyint(4) DEFAULT '0',
  `metaTitle` varchar(150) DEFAULT NULL,
  `metaDescription` varchar(800) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `centerRegions` tinyint(4) DEFAULT '0',
  `customLoginHtml` text,
  `mailPlatform` int(10) unsigned DEFAULT '0',
  `mailUser` varchar(250) DEFAULT NULL,
  `mailUserPassword` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`siteID`),
  UNIQUE KEY `u_siteName` (`siteName`),
  KEY `fk_Sites__siteType_SiteTypes__typeID` (`siteType`),
  CONSTRAINT `Sites_ibfk_1` FOREIGN KEY (`siteType`) REFERENCES `sitetypes` (`typeID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sitetypes`
--

DROP TABLE IF EXISTS `sitetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitetypes` (
  `typeID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `notes` text,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slides` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `caption` text,
  `imageId` bigint(20) DEFAULT NULL,
  `learn_more_link` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `customContent` longtext,
  `learnMoreText` varchar(100) DEFAULT NULL,
  `videoId` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `statusID` bigint(20) unsigned NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`statusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemareas`
--

DROP TABLE IF EXISTS `systemareas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemareas` (
  `areaId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `areaName` varchar(50) DEFAULT NULL,
  `enabled` bit(1) DEFAULT b'0',
  PRIMARY KEY (`areaId`),
  UNIQUE KEY `areaName` (`areaName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemsettings`
--

DROP TABLE IF EXISTS `systemsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemsettings` (
  `settingsID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `settingsName` varchar(45) NOT NULL,
  `settingsValue` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`settingsID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemunits`
--

DROP TABLE IF EXISTS `systemunits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemunits` (
  `unitID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unitName` varchar(50) DEFAULT NULL,
  `singularName` varchar(50) NOT NULL DEFAULT '',
  `pluralName` varchar(50) NOT NULL DEFAULT '',
  `defaultFormId` bigint(20) unsigned DEFAULT '0',
  `defaultGridId` bigint(20) unsigned DEFAULT '0',
  `enabled` bit(1) DEFAULT b'0',
  `defaultFormPage` varchar(200) DEFAULT NULL,
  `singularIcon` varchar(100) NOT NULL,
  `pluralIcon` varchar(100) NOT NULL,
  `lookupDisplayPropertyId` bigint(20) unsigned DEFAULT '0',
  `lookupPropertyPrimaryKey` bigint(20) unsigned DEFAULT '0',
  `persistenceClass` varchar(45) DEFAULT '',
  `lookupPropertyPrimaryKey2` bigint(20) unsigned DEFAULT '0',
  `tableName` varchar(80) DEFAULT NULL,
  `locked` bit(1) DEFAULT b'0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`unitID`),
  KEY `SystemUnits_idx` (`defaultFormId`),
  KEY `SystemUnits_idx2` (`defaultGridId`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `systemunitsareas`
--

DROP TABLE IF EXISTS `systemunitsareas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemunitsareas` (
  `systemUnitId` bigint(20) unsigned NOT NULL,
  `systemAreaId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`systemUnitId`,`systemAreaId`),
  KEY `fk_systemunitsareas_2_idx` (`systemAreaId`),
  CONSTRAINT `fk_systemunitsareas_1` FOREIGN KEY (`systemUnitId`) REFERENCES `systemunits` (`unitID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_systemunitsareas_2` FOREIGN KEY (`systemAreaId`) REFERENCES `systemareas` (`areaId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tabs`
--

DROP TABLE IF EXISTS `tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabs` (
  `tabId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tabName` varchar(50) NOT NULL,
  `formId` bigint(20) unsigned DEFAULT NULL,
  `referencedUnitProperty` bigint(20) unsigned DEFAULT NULL,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  `gridId` bigint(20) unsigned DEFAULT NULL,
  `sequence` bigint(20) unsigned NOT NULL,
  `customQuery` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`tabId`),
  KEY `new_fk_constraint` (`formId`),
  KEY `fk_tabs_unitproperty` (`referencedUnitProperty`),
  KEY `fk_tabs_unitid` (`unitId`),
  CONSTRAINT `fk_tabs_unitid` FOREIGN KEY (`unitId`) REFERENCES `systemunits` (`unitID`),
  CONSTRAINT `fk_tabs_unitproperty` FOREIGN KEY (`referencedUnitProperty`) REFERENCES `unitproperties` (`propertyId`),
  CONSTRAINT `new_fk_constraint100` FOREIGN KEY (`formId`) REFERENCES `forms` (`formId`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tabtypes`
--

DROP TABLE IF EXISTS `tabtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabtypes` (
  `typeId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `body` text NOT NULL,
  `author` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `transID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(12,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `paymentType` varchar(50) DEFAULT NULL,
  `accountNumber` varchar(50) DEFAULT NULL,
  `expiration` varchar(10) DEFAULT NULL,
  `cvv2` varchar(10) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `payflowTransactionType` varchar(10) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `payflowResult` varchar(45) DEFAULT NULL,
  `paymentResponseMessage` varchar(100) DEFAULT NULL,
  `paymentResponse` varchar(200) DEFAULT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `payflowAuthCode` varchar(45) DEFAULT NULL,
  `payflowPnref` varchar(45) DEFAULT NULL,
  `payflowAvsAddr` varchar(1) DEFAULT NULL,
  `payflowCvvMatch` varchar(1) DEFAULT NULL,
  `payflowAvsZip` varchar(1) DEFAULT NULL,
  `payflowIavs` varchar(1) DEFAULT NULL,
  `payflowStatus` varchar(100) DEFAULT NULL,
  `payflowFraudTriggered` tinyint(1) DEFAULT NULL,
  `phoneNumber` varchar(45) DEFAULT NULL,
  `emailAddress` varchar(100) DEFAULT NULL,
  `purchasedItems` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`transID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unitproperties`
--

DROP TABLE IF EXISTS `unitproperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unitproperties` (
  `propertyId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `propertyName` varchar(50) DEFAULT NULL,
  `propertyType` bigint(20) unsigned DEFAULT NULL,
  `bundleLabelId` varchar(50) DEFAULT NULL,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  `lookupUnitId` bigint(20) unsigned DEFAULT '0',
  `lookupGridId` bigint(20) unsigned DEFAULT '0',
  `fakeLookup` bit(1) DEFAULT b'0',
  `booleanWidget` varchar(45) DEFAULT NULL,
  `textLength` int(11) DEFAULT '0',
  `codeType` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`propertyId`),
  KEY `UnitProperties_idx2` (`propertyType`),
  KEY `UnitProperties_idx3` (`unitId`),
  CONSTRAINT `UnitProperties_ibfk_1` FOREIGN KEY (`unitId`) REFERENCES `systemunits` (`unitID`),
  CONSTRAINT `UnitProperties_ibfk_2` FOREIGN KEY (`propertyType`) REFERENCES `unitpropertytypes` (`typeId`)
) ENGINE=InnoDB AUTO_INCREMENT=664 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unitpropertylistitems`
--

DROP TABLE IF EXISTS `unitpropertylistitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unitpropertylistitems` (
  `itemId` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `unitPropertyId` bigint(20) unsigned NOT NULL,
  `itemValue` varchar(100) NOT NULL,
  `displayValue` varchar(100) NOT NULL,
  PRIMARY KEY (`itemId`),
  KEY `FK_unitpropertylistitems_1` (`unitPropertyId`),
  CONSTRAINT `FK_unitpropertylistitems_1` FOREIGN KEY (`unitPropertyId`) REFERENCES `unitproperties` (`propertyId`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unitpropertytypes`
--

DROP TABLE IF EXISTS `unitpropertytypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unitpropertytypes` (
  `typeId` bigint(20) unsigned NOT NULL,
  `typeName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unitsiteassociations`
--

DROP TABLE IF EXISTS `unitsiteassociations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unitsiteassociations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `siteId` bigint(20) unsigned DEFAULT NULL,
  `unitId` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unitsiteassociations_sites_siteID_fk` (`siteId`),
  KEY `unitsiteassociations_systemunits_unitID_fk` (`unitId`),
  CONSTRAINT `unitsiteassociations_sites_siteID_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`),
  CONSTRAINT `unitsiteassociations_systemunits_unitID_fk` FOREIGN KEY (`unitId`) REFERENCES `systemunits` (`unitID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `urlredirects`
--

DROP TABLE IF EXISTS `urlredirects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `urlredirects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `redirectFrom` varchar(255) NOT NULL,
  `redirectTo` varchar(255) NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  `enabled` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `groupID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parentID` bigint(20) unsigned DEFAULT '0',
  `description` text,
  `groupName` varchar(50) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userhistory`
--

DROP TABLE IF EXISTS `userhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userhistory` (
  `userHistoryID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `loginDate` datetime DEFAULT NULL,
  `urlLogged` varchar(40) NOT NULL DEFAULT '',
  KEY `userHistoryID` (`userHistoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpermissions`
--

DROP TABLE IF EXISTS `userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpermissions` (
  `permissionID` bigint(20) unsigned NOT NULL,
  `moduleID` varchar(255) DEFAULT NULL,
  `accessLevel` bigint(20) unsigned DEFAULT NULL,
  `instance` text,
  `permissionSequence` bigint(20) unsigned DEFAULT NULL,
  `userID` bigint(20) unsigned DEFAULT NULL,
  `realm` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`permissionID`),
  KEY `fk_UserPermissions__userID_Users__userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userName` varchar(80) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `userSecretQuestion` int(5) DEFAULT NULL,
  `userSecretAnswer` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `firstName` varchar(80) DEFAULT NULL,
  `middleName` varchar(80) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT '0',
  `globalAdmin` tinyint(4) DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userID` (`userID`),
  UNIQUE KEY `userEmail` (`email`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userstogroups`
--

DROP TABLE IF EXISTS `userstogroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userstogroups` (
  `groupId` bigint(20) unsigned NOT NULL,
  `userId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`groupId`,`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `width` bigint(20) unsigned DEFAULT NULL,
  `mimetypeid` bigint(20) unsigned DEFAULT NULL,
  `filePath` varchar(250) DEFAULT NULL,
  `customContent` text,
  `header` text,
  `footer` text,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `videoAsBackground` tinyint(4) DEFAULT '0',
  `backgroundType` int(11) DEFAULT '0',
  `videoPlatform` int(11) DEFAULT '0',
  `platformVideoId` varchar(45) DEFAULT NULL,
  `overlayCaption` text,
  `fallbackImageId` bigint(20) unsigned DEFAULT NULL,
  `minHeight` int(11) DEFAULT '0',
  `showHeader` tinyint(4) DEFAULT '1',
  `showFooter` tinyint(4) DEFAULT '1',
  `overlayColor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videosites`
--

DROP TABLE IF EXISTS `videosites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videosites` (
  `videoId` bigint(20) unsigned NOT NULL,
  `siteId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`videoId`,`siteId`),
  KEY `fk_videosites_2_idx` (`siteId`),
  CONSTRAINT `fk_videosites_1` FOREIGN KEY (`videoId`) REFERENCES `videos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_videosites_2` FOREIGN KEY (`siteId`) REFERENCES `sites` (`siteID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videotags`
--

DROP TABLE IF EXISTS `videotags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videotags` (
  `videoId` bigint(20) unsigned NOT NULL,
  `tagId` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`videoId`,`tagId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vocabterms`
--

DROP TABLE IF EXISTS `vocabterms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vocabterms` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `vocab_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `parentTermId` bigint(20) DEFAULT '0',
  `sequenceIndex` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Vocabulary Key_idx` (`vocab_id`),
  CONSTRAINT `fk_vocabterms_1` FOREIGN KEY (`vocab_id`) REFERENCES `vocabularies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vocabularies`
--

DROP TABLE IF EXISTS `vocabularies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vocabularies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'Iberis'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-11 21:53:02
